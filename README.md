This is a fun 3D game I worked on as a challenge. It allows you to select a vehicle of your choice from a list and afterwards roam the 3D city, 
following traffic rules. This was my attempt at eventually working on an AI that would allow for self-driving cars. To generate the 3D world 
I used "3D Maps Platform" API.

You can have a go at it, simply Download Unity 3D and run the game in development mode. Feel free to also port the project to whatever platform you'd like
to try the game in since Unity gives you that option.

** Game screenshots will be uploaded